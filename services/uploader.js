const path = require('path')
const fu = require('../utils/file')

const uploadFile = async (info, uploads) => {
  const { album } = info
  const directory = path.join(process.cwd(), 'albums', album)

  await fu.ensureDirectory(directory)
  const result = []

  for (const upload of uploads) {
    const { name } = upload
    const destination = path.join(directory, name)
    upload.mv(destination)

    const album = destination.split('albums')[1].split('\\')[1]

    result.push({
      // album: album.replace(/\b(\w)/g, s => s.toUpperCase()),
      // path: destination.replace(process.cwd(), ''),

      album: fu.capitalizeFirstLetter(album),
      name,
      path: `http://localhost:8888/photos/${album}/${name}`
    })
  }

  return result
}

module.exports = { uploadFile }
